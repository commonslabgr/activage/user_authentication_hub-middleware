#!/bin/bash

echo "Launching Keycloak Server..."
/home/pi/keycloak-6.0.1/bin/standalone.sh &
sleep 20

#Start HTTPS server
cd /home/pi/user_authentication_hub-middleware
python3 httpsServer.py &

#Move mouse out of view
DISPLAY=:0 xdotool mousemove 0 0

echo "Launching Chromium..."
DISPLAY=:0 chromium-browser --kiosk file:///home/pi/user_authentication_hub-middleware/ui/index.html
#WOQUAZ For callbacks to python httpsserver when registering new users
#DISPLAY=:0 chromium-browser --kiosk --disable-site-isolation-trials --disable-web-security --user-data-dir="/home/pi/chromium" --ignore-certificate-errors file:///home/pi/user_authentication_hub-middleware/ui/index.html

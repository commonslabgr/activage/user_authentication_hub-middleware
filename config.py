import  os
from requests.auth import HTTPBasicAuth
from easydict import EasyDict as edict
import socket    

config = edict()

config.DeploymentSite='WOQUAZ'
config.logFileController='logs/uah_controller.log'
config.logFileServer='logs/httpsServer.log'
config.keyfile='certs/keycloak.key'
config.certfile='certs/keycloak.pem'
config.uirefreshinterval = 5
config.sensinact_ip='193.48.18.245'
config.sensinact_port='8081'
config.home_id='PTL_01-home'

hostname = socket.gethostname()    
IPAddr = socket.gethostbyname(hostname) 

config.JsonHeader={'Content-type': 'application/json'}
config.TextHeader={'Content-type': 'text/plain', 'Accept': 'text/plain'}
config.Auth=HTTPBasicAuth('top07', 'uLiveTOP07Lis')  # universaal rest api username/password
config.target_uSpace='uah_wqz07'  # space name
config.publisher_instanceID='uah_wqz07_publisher'
config.serverIP='10.99.68.69'   # target universaal instance ip address 
config.serverPort='9000' #  universaal api  port 
config.base_dir='/home/pi/user_authentication_hub-middleware/uLive_Requests/data/'
config.data_dir_space='space.txt'
config.data_dir_publisher='publisher.txt'
config.data_dir_published_messsage='published_messsage.txt'

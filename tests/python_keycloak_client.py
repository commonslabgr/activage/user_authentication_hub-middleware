from keycloak.realm import KeycloakRealm


realm = KeycloakRealm(server_url='https://uah.local:8443/auth/realms/', realm_name='UAH')

oidc_client = realm.open_id_connect(client_id='raspberrypi',
                                    client_secret='31e1e909-e37a-48d5-8c76-74d7ca8e3b60')

print (oidc_client.authorization_code)
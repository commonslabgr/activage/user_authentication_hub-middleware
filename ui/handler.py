import os
import gettext
from threading import Timer
from config import *
from bs4 import BeautifulSoup

#French translation
#fr = gettext.translation('handler', localedir='locale', languages=['fr'])
#fr.install()
#German translation
#de = gettext.translation('handler', localedir='ui/locale', languages=['de'])
#de.install()
#Return to English
_ = lambda s: s

filename = '/home/pi/user_authentication_hub-middleware/ui/index.html'
soup = BeautifulSoup(open(filename), 'html5lib')

def HideButtons():
    soup.find("progress", {"id": "progressBar"})['style'] = "display:none"
    #soup.find("button", {"id": "leftbtn"})['style'] = "display:none"
    #soup.find("button", {"id": "rightbtn"})['style'] = "display:none"

def saveHTML(html):
    html.prettify(formatter="html5")
    with open(filename, "w", encoding='utf-8') as file:
        file.write(str(html))

#Refresh chromium
def RefreshBrowser():
    cmd = "DISPLAY=:0 xdotool key F5"
    os.system(cmd)

def ShowRunning():
    soup.find("div", {"id": "message"}).string.replace_with(_('Service is running'))
    #Update status icon
    soup.find("img", {"id": "imgstatus"})['src'] = 'assets/empty.png'
    soup.find("div", {"id": "footer"}).string.replace_with(_('UAH module is running...'))
    HideButtons()
    saveHTML(soup)
    RefreshBrowser()

def ShowError(error):
    return 0


def setMessage(message):
    #Update message
    soup.find("div", {"id": "message"}).string.replace_with(message)
    #Update status icon
    soup.find("img", {"id": "imgstatus"})['src'] = 'assets/success.png'
    saveHTML(soup)
    RefreshBrowser()
    t = Timer(config.uirefreshinterval, ShowRunning)
    t.start()    


def msgPleaseSwypeCard():
    #Update status icon
    soup.find("img", {"id": "imgstatus"})['src'] = 'assets/empty.png'    
    soup.find("div", {"id": "message"}).string.replace_with(_('Please place your card close to the reader...'))
    HideButtons()
    saveHTML(soup)
    RefreshBrowser()
    t = Timer(2*config.uirefreshinterval, ShowRunning)
    t.start()    


def AuthenticationSuccess(username):
    #Update message
    soup.find("div", {"id": "message"}).string.replace_with(username+_(' succesfully authenticated'))
    #Update status icon
    soup.find("img", {"id": "imgstatus"})['src'] = 'assets/success.png'
    HideButtons()
    saveHTML(soup)
    RefreshBrowser()
    t = Timer(config.uirefreshinterval, ShowRunning)
    t.start()    

def Logout():
    #Update message
    soup.find("div", {"id": "message"}).string.replace_with(_('User has been logged out'))
    #Update status icon
    soup.find("img", {"id": "imgstatus"})['src'] = 'assets/success.png'
    HideButtons()
    saveHTML(soup)
    RefreshBrowser()
    t = Timer(config.uirefreshinterval, ShowRunning)
    t.start()

def AuthenticationFail(error):
    #Update message
    soup.find("div", {"id": "message"}).string.replace_with(_('Authentication failed: ')+error)
    #Update status icon
    soup.find("img", {"id": "imgstatus"})['src'] = 'assets/fail.png'
    HideButtons()
    saveHTML(soup)
    RefreshBrowser()
    t = Timer(config.uirefreshinterval, ShowRunning)
    t.start()
    
def NewUser(username,userid):
    soup.find("progress", {"id": "progressBar"})['style'] = "display:block"
    #soup.find("button", {"id": "leftbtn"})['style'] = "display:block"
    #soup.find("button", {"id": "rightbtn"})['style'] = "display:block"
    #soup.find("button", {"id": "leftbtn"}).string.replace_with(_('Finish'))
    #soup.find("button", {"id": "rightbtn"}).string.replace_with(_('Next'))
    soup.find("div", {"id": "message"}).string.replace_with(_('Please swipe new card to register with ')+username)
    soup.find("div", {"id": "username"}).string.replace_with(username)
    soup.find("div", {"id": "userid"}).string.replace_with(userid)
    saveHTML(soup)
    RefreshBrowser()

def RegistrationSuccess(username):
    soup.find("div", {"id": "message"}).string.replace_with(username+_(' registered successfully.'))
    HideButtons()
    saveHTML(soup)
    RefreshBrowser()
    t = Timer(config.uirefreshinterval, ShowRunning)
    t.start()
import os
import sys
import time
import signal
import subprocess
import logging
import sys
import threading
import keycloak_client
sys.path.append('ui')
import handler
sys.path.append('cardReader')
import cardreader
sys.path.append('uLive_Requests')
import helper
import configparser
from config import *

run_once = False

sensinactUserAuthorized = False

#create logger
logger = logging.getLogger(__name__)
logger.setLevel(logging.INFO)

# create a file handler
loghandler = logging.FileHandler(config.logFileController)
loghandler.setLevel(logging.INFO)

# create a logging format
formatter = logging.Formatter('%(asctime)s - %(levelname)s - %(message)s')
loghandler.setFormatter(formatter)

# add the handlers to the logger
logger.addHandler(loghandler)

#Read NFC card and get ID
def ReadCard():
    line = cardreader.read()
    line = line.strip()
    return line

def ReadCardID():
    id = cardreader.readID()
    return id

#Get Token ID from keycloak
def oidc_getIDToken():
    global sensinactUserAuthorized
    logger.info('oidc_getIDToken')
    #Call UI module to ask user to authenticate
    handler.msgPleaseSwypeCard()
    #ReadCard
    line = ReadCard()
    try:
        username, password = line.split(' ')
    except ValueError:
        sensinactUserAuthorized = False
        handler.AuthenticationFail('Invalid card')
        logger.info('Invalid card')
        return 'Error'
    else:
        sensinactUserAuthorized = True
        logger.info('Request token for '+username)
        token = keycloak_client.getToken(username, password)
        handler.AuthenticationSuccess(username)
        return token

#Get Access Token from keycloak
def oidc_getAccessToken():
    global sensinactUserAuthorized
    logger.info('oidc_getAccessToken')
    #Call UI module to ask user to authenticate
    handler.msgPleaseSwypeCard()
    #ReadCard
    line = ReadCard()
    try:
        username, password = line.split(' ')
    except ValueError:
        sensinactUserAuthorized = False
        handler.AuthenticationFail('Invalid card')
        logger.info('Invalid card')
        return 'Error'
    else:
        sensinactUserAuthorized = True
        logger.info('Request token for '+username)
        token = keycloak_client.getToken(username, password)
        handler.AuthenticationSuccess(username)
        return token['access_token']


#Get Token ID from keycloak
def oidc_getUserInfo():
    global sensinactUserAuthorized
    logger.info('oidc_getUserInfo')
    #Call UI module to ask user to authenticate
    handler.msgPleaseSwypeCard()
    #ReadCard
    line = ReadCard()
    try:
        username, password = line.split(' ')
    except ValueError:
        sensinactUserAuthorized = False
        logger.info('Invalid card')
        handler.AuthenticationFail('Invalid card')
        return 'Error'
    else:
        sensinactUserAuthorized = True
        token = keycloak_client.getToken(username, password)
        token = keycloak_client.getUserInfo(token['access_token'])
        handler.setMessage('Name:'+token['given_name']+' Email:'+token['email'])
        return token
    

#Get Token ID from keycloak
def oidc_Logout():
    global sensinactUserAuthorized
    sensinactUserAuthorized = False
    logger.info('oidc_Logout')
    #Call UI module to ask user to authenticate
    handler.msgPleaseSwypeCard()
    #ReadCard
    line = ReadCard()
    try:
        username, password = line.split(' ')
    except ValueError:
        logger.info('Invalid card')
        handler.AuthenticationFail('Invalid card')
        return 'Error'
    else:
        token = keycloak_client.getToken(username, password)
        handler.Logout()    
        return 'Logged out'

def Cardreader_timeout(signum, frame):
    logger.info("Timeout")
    raise Exception("Timeout")


def NewUserRegistration(name,userid):
    if (config.DeploymentSite == "WOQUAZ"):
        return

    logger.info('Check how many card the user has registered')
    users = configparser.ConfigParser()
    users.read('Users.ini')
    num_RegisteredCards = 0
    for (key, val) in users.items('USERS'):
        print (val)
        if (val == userid):
            num_RegisteredCards += 1
    if (num_RegisteredCards >= 3):
        logger.info(name+' has already 3 cards registered')
        return

    while num_RegisteredCards < 3:
        logger.info('NewUserRegistration ask for card swype')
        handler.NewUser(name,userid)
        #cardid = str(ReadCardID())
        signal.signal(signal.SIGALRM, Cardreader_timeout)
        signal.alarm(10)
        try:
            cardid = str(ReadCardID())
            logger.info("ReadCard: "+cardid)
        except Exception:
            handler.ShowRunning()
            logger.info("Timeout")
            return
        
        #If user exists, check how many registered cards they have
        if users.has_option('USERS',cardid):
            num_RegisteredCards = 0
            logger.info('User exists')
            for (key, val) in users.items('USERS'):
                if (val == userid):
                    num_RegisteredCards += 1
            if (num_RegisteredCards < 3):
                logger.info('Add '+name+' to card '+str(num_RegisteredCards)+':'+cardid)
                users.set('USERS',cardid,userid)
                with open('Users.ini', 'w') as configfile:    # save
                    users.write(configfile)
        #If user does not exist, add them
        else:
            logger.info('Add NEW user '+name+' to card:'+cardid)
            users.set('USERS',cardid,userid)
            with open('Users.ini', 'w') as configfile:    # save
                users.write(configfile)

        handler.RegistrationSuccess(name)
        time.sleep(2)

    #Registration finished, update UI
    handler.ShowRunning()
    return name+' registered'

def Home():
    logger.info('')
    handler.ShowRunning()

def CheckforNewRegistrations():
    global run_once

    if run_once:
        logger.info('NewRegistration has already run')
        return

    logger.info('CheckforNewRegistrations')
    
    '''  Joint the uSpace  '''
    helper.JointuSpace()
    ''' Register the publisher ''' 
    helper.registerPublisher()
    
    run_once = True
    

def ReadCardNotifyUniversAAL():
    logger.info('ReadCardNotifyUniversAAL')
    counter = int(round(time.time() * 1000))
    configuredCards = configparser.ConfigParser()
    configuredCards.read('CardsConf.ini')
    cards = dict(configuredCards.items('CARDS'))
    messages = dict(configuredCards.items('MESSAGES'))
    while True:
        id = str(cardreader.readIDContinuously())
        if ((id != "") and (id != "None")):
            try:
                result = helper.userIdentified(logger, cards[id], counter)
                logger.info("Notified uLive about "+cards[id]+": "+str(result))
                counter +=1
                # update UI
                handler.setMessage(messages[cards[id]])
            except KeyError:
                logger.info("Unknown card: "+id)
                handler.setMessage(messages['default'])
            time.sleep(7)

        handler.ShowRunning()
        time.sleep(0.5)  
      



def main():
    logger.info('Show service running')
    handler.ShowRunning()
    if (config.DeploymentSite == "WOQUAZ"):
        #Check for new users from UniversAAL
        #CheckforNewRegistrations()
    
        '''  Joint the uSpace  '''
        result = helper.uSpaceJoin(logger)
        logger.info('Connection to uLive: '+str(result))

        ''' Register the publisher ''' 
        result = helper.registerPublisher(logger)
        logger.info('Registering publisher with uLive: '+str(result))

        ReadCardNotifyUniversAAL()
        #t = threading._start_new_thread(ReadCardNotifyUniversAAL,("Thread",))

if __name__ == "__main__":
    main()

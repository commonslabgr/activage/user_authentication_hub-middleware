import ssl
from keycloak import KeycloakOpenID
from keycloak import KeycloakGetError

ssl.match_hostname = lambda cert, hostname: True

# Configure client
#KeycloakOpenID(server_url="https://uah.local:8443/auth/realms/UAH/protocol/openid-connect/auth",
keycloak_openid = KeycloakOpenID(server_url="https://uah.local:8443/auth/",
                    client_id="raspberrypi",
                    realm_name="UAH",
                    client_secret_key="31e1e909-e37a-48d5-8c76-74d7ca8e3b60")

# Get WellKnow
#config_well_know = keycloak_openid.well_know()
#print (config_well_know)

# Get Token
def getToken(username, password):
    try:
        token = keycloak_openid.token(username, password)
        print (token)
        return token
    except KeycloakGetError:    
        print ('ERROR')
        print(KeycloakGetError)
        return 'Error'

# Get Userinfo
def getUserInfo(access_token):
    userinfo = keycloak_openid.userinfo(access_token)
    return userinfo

# Refresh token
def getRefreshToken(refresh_token):
    token = keycloak_openid.refresh_token(refresh_token)
    return token

# Logout
def logout(refresh_token):
    keycloak_openid.logout(refresh_token)

# Get Certs
#certs = keycloak_openid.certs()

# Get RPT (Entitlement)
#token = keycloak_openid.token("user", "password")
#rpt = keycloak_openid.entitlement(token['access_token'], "resource_id")

# Instropect RPT
#token_rpt_info = keycloak_openid.introspect(keycloak_openid.introspect(token['access_token'], rpt=rpt['rpt'], token_type_hint="requesting_party_token"))

# Introspect Token
#token_info = keycloak_openid.introspect(token['access_token'])

# Decode Token
#KEYCLOAK_PUBLIC_KEY = "secret"
#options = {"verify_signature": True, "verify_aud": True, "exp": True}
#token_info = keycloak_openid.decode_token(token['access_token'], key=KEYCLOAK_PUBLIC_KEY, options=options)

# Get permissions by token
#token = keycloak_openid.token("user", "password")
#keycloak_openid.load_authorization_config("example-authz-config.json")
#policies = keycloak_openid.get_policies(token['access_token'], method_token_info='decode', key=KEYCLOAK_PUBLIC_KEY)
#permissions = keycloak_openid.get_permissions(token['access_token'], method_token_info='introspect')

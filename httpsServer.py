#!/usr/bin/env python3

import socketserver
import sys
import json
import logging
import threading
import time
import requests
import urllib.parse
from urllib.parse import parse_qs
from http.server import BaseHTTPRequestHandler
import uah_controller
import ssl
sys.path.append('cardReader')
import cardreader
import handler
from config import *

cardInserted = False
#create logger
logger = logging.getLogger(__name__)
logger.setLevel(logging.INFO)

# create a file handler
loghandler = logging.FileHandler(config.logFileServer)
loghandler.setLevel(logging.INFO)

# create a logging format
formatter = logging.Formatter('%(asctime)s - %(levelname)s - %(message)s')
loghandler.setFormatter(formatter)

# add the handlers to the logger
logger.addHandler(loghandler)

class httpsHandler(BaseHTTPRequestHandler):
    def _set_json_headers(self):
        self.send_response(200)
        self.send_header('Content-type', 'application/json')
        self.end_headers()

    def _set_html_headers(self):
        self.send_response(200)
        self.send_header('Content-type', 'text/html')
        self.end_headers()

    def _set_image_headers(self):    
        self.send_response(200)
        self.send_header('Content-type', 'image/png')
        self.end_headers()

    def do_GET(self):
        if self.path == '/auth':
            logger.info('Received /auth')
            self._set_json_headers()
            data = uah_controller.oidc_getAccessToken()
            print (data)
            self.wfile.write(json.dumps(data).encode('utf-8'))
        elif self.path == '/token':
            logger.info('Received /token')
            self._set_json_headers()
            data = uah_controller.oidc_getIDToken()
            print (data)
            self.wfile.write(json.dumps(data).encode('utf-8'))
        elif self.path == '/userinfo':
            logger.info('Received /userinfo')
            self._set_json_headers()
            data = uah_controller.oidc_getUserInfo()
            print (data)
            self.wfile.write(json.dumps(data).encode('utf-8'))
        elif self.path == '/logout':       
            logger.info('Received /logout') 
            self._set_html_headers()
            data = uah_controller.oidc_Logout()
            self.wfile.write(b'Logout')
        elif self.path.startswith('/newusers'):
            logger.info('Received /newusers')
            print(self.path)
            parsed = urlparse(self.path)
            username = parse_qs(parsed.query)['name']
            #print(username)
            userid = parse_qs(parsed.query)['id']
            #print(userid)
            #self._set_html_headers()
            #data = uah_controller.NewUserRegistration('NewUser')
            #print (data)
        elif self.path == '/home':
            logger.info('Received /home')
            self._set_html_headers()
            uah_controller.Home()
        elif self.path == '/.well-known/openid-configuration':
            logger.info('Received /.well-known/openid-configuration')
            self._set_json_headers()
            #data = json.loads('openid-configuration.json')
            #data = open('openid-configuration.json','r') 
            json_file = open('openid-configuration.json','rb')
            jsondata = json_file.read()
            self.wfile.write(jsondata)
        elif self.path.__contains__('.png'):
            imgname = self.path
            print ("Image requested is: ", imgname[1:])
            imgfile = open(imgname[1:], 'rb').read()
            self._set_image_headers()
            self.wfile.write(imgfile)
        #For testing new users from UniversAAL (WOQUAZ)    
        #elif self.path == '/testusers':
        #    uah_controller.CheckforNewRegistrations()
        else:
            self._set_html_headers()
            #Show home page with choices for demo
            f = open('ui/home.html', 'r')
            home_contents = f.read()
            self.wfile.write(home_contents.encode('utf-8'))

logger.info('Staring HTTPS Server...')
socketserver.TCPServer.allow_reuse_address = True
httpd = socketserver.TCPServer(("", 4443), httpsHandler )
httpd.socket = ssl.wrap_socket (httpd.socket,
                                keyfile=config.keyfile,
                                certfile=config.certfile,
                                server_side=True)

handler.ShowRunning()

def DetectCardRemoval(self):
    global cardInserted
    header = {'Content-Type': 'application/json','Accept':'application/json'}
    url = 'http://'+config.sensinact_ip+':'+config.sensinact_port+'/sensinact/providers/'+config.home_id+'/services/control/resources/authorize/ACT'
    
    while True:
        time.sleep(0.2)
        cardreader.readContinuously()
        time.sleep(0.5)
        line = cardreader.readContinuously()
        #print('Line: '+str(line))
        if (str(line) == 'None') and (cardInserted) and (uah_controller.sensinactUserAuthorized):
            cardInserted = False
            content = {"parameters": [{"name": "state","value": cardInserted,"type": "boolean"}]}
            #print('Cardremoved')
            #print(content)
            requests.post(url,data=content,headers=header)
        elif(str(line) != 'None') and not (cardInserted) and (uah_controller.sensinactUserAuthorized):
            cardInserted = True
            content = {"parameters": [{"name": "state","value": cardInserted,"type": "boolean"}]}
            #print('CardInserted')
            #print(content)
            requests.post(url,data=content,headers=header)
        

if (config.DeploymentSite == "ISERE"):
    t = threading._start_new_thread(DetectCardRemoval,("Thread",))
    
httpd.serve_forever()

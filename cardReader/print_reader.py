#!/usr/bin/env python

import sys
import RPi.GPIO as GPIO
from mfrc522 import SimpleMFRC522

reader = SimpleMFRC522(bus=1, device=2, pin_mode=GPIO.BCM, pin_rst=26)

try:
	#(id, text) = reader.read_no_block()
	(id, text) = reader.read()
	print(id)
	print(text)

finally:
	GPIO.cleanup()

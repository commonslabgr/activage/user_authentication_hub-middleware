#!/usr/bin/env python

import sys
import RPi.GPIO as GPIO
from mfrc522 import SimpleMFRC522

reader = SimpleMFRC522(bus=1, device=2, pin_mode=GPIO.BCM, pin_rst=26)

def read():
	try:
		(id, text) = reader.read()
		#print(id)
		#print(text)

	finally:
		GPIO.cleanup()
	
	return text

def readID():
	try:
		(id, text) = reader.read()

	finally:
		GPIO.cleanup()
	
	return id

def readIDContinuously():
	try:
		(id, text) = reader.read_no_block()
	finally:
		GPIO.cleanup()
		
	return id
	
def readContinuously():
	try:
		(id, text) = reader.read_no_block()
	finally:
		GPIO.cleanup()
		
	return text
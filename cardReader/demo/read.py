#!/usr/bin/env python

import sys
import RPi.GPIO as GPIO
from mfrc522 import SimpleMFRC522

reader = SimpleMFRC522()

try:
	(id, text) = reader.read()
	print(id)
	print(text)

finally:
	GPIO.cleanup()

auth = 1

if id == 319739053328:
	auth = 0
	print("OK")
elif id == 373124378699:
	auth = 0
	print("OK")
else:
	print("UNKOWN")
	
sys.exit(auth)
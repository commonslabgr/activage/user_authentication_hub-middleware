[![License: GPL v3](https://img.shields.io/badge/License-GPL%20v3-blue.svg)](https://www.gnu.org/licenses/gpl-3.0)

**License**

GNU GENERAL PUBLIC LICENSE Version 3


**Description**

The User Authentication Hub middleware uses the Keycloak server in order to provide OpenID user authentication services to devices in a local network through simple user authentication, allowing the user to simply swipe a pre-registered RFID card in order to authenticate.
It is part of the Modular and Open-Source User Authentication Hub project created by [CommonsLab](http://commonslab.gr) that was accepted on the 1st Open Call of the EU LSP [ACTIVAGE project](http://www.activageproject.eu)


**Installation**
The following instructions are for setting up UAH on a Raspberry Pi 3 B/B+.

* Download and flash Raspbian with Desktop
* Boot Raspbian on Pi 3
* Install [Keycloak](https://www.keycloak.org/)
* Install Python3
* Install xdotool
* Python dependencies
  *   easydict
  *   urllib3
  *   mfrc522 - [patched](https://gitlab.com/commonslabgr/activage/SimpleMFRC522)
  *   [python-keycloak](https://github.com/marcospereirampj/python-keycloak) 
* Copy code under /home/pi/user_authentication_hub-middleware/

Raspberry Pi 3 images
* For DS ISERE Use case with Keycloak and LDAP [(3.3GB compressed, 16GB uncompressed)](https://drive.google.com/open?id=1sqaDBmkTJ2U6lrRHfOtkPsWpOC_0TZKu)
* For DS WOQUAZ Use case with alert on UniversAAL [(3.3GB compressed, 16GB uncompressed)](https://drive.google.com/open?id=14Rci63d8tM1xrmx9xg77bABQqOGf7xAd)


**Set Up from image**
If you are using a pre-installed UAH or have flashed the image the first thing you may want to do is get access to your device through SSH.
To do that you can connect the UAH through ethernet to your local network with a DHCP and then access the pi with ssh pi@uah.local and the password we have provided.
Then edit the /etc/wpa_supplicant/wpa_supplicant.conf to update the WiFi for the UAH to connect to your local WiFi. [Raspberry Pi Setting WiFi up via the command line](https://www.raspberrypi.org/documentation/configuration/wireless/wireless-cli.md)


**Configuration**

* [Keycloak](https://www.keycloak.org/)
  * Install mysql
  * Download JDBC connector
  * Configure [classpath](https://dev.mysql.com/doc/connector-j/8.0/en/connector-j-binary-installation.html)
  * Add "JAVA_OPTS="$JAVA_OPTS -XX:+CreateMinidumpOnCrash"" to standalone.conf to fix failed core dumps of JVM
  * Set up [SSL](https://www.keycloak.org/docs/latest/server_installation/index.html#setting-up-https-ssl)
  * After creating jks convert to pem
    *   keytool -importkeystore -srckeystore keycloak.jks -destkeystore foo.p12 -srcstoretype jks -deststoretype pkcs12
    *   openssl pkcs12 -in keycloak.p12 -out keycloak.pem
    *   mkdir /usr/share/ca-certificates/local
    *   cp keycloak.crt /usr/share/ca-certificates/local/keycloak.crt
    *   cp keycloak.pem /usr/share/ca-certificates/local/keycloak.pem
    *   sudo dpkg-reconfigure ca-certificates
  * In Keycloak admin
    * Create new realm "UAH"
    * Create new client "raspberrypi"
    * Add user federation with LDAP server
  
* Edit user_authentication_hub-middleware/config.py
  * set DeploymentSite as ISERE or WOQUAZ depending on the use case
  * ISERE specific for notifying Sensinact for card removal
    * sensinact_ip
    * sensinact_port
    * home_id
  * WOQUAZ specific for communication with UniversAAL
    * JsonHeader
    * TextHeader
    * Auth
    * target_uSpace
    * caller_instanceID
    * publisher_instanceID
    * serverIP
    * serverPort
    * base_dir
    * data_dir_space
    * data_dir_caller
    * data_dir_publisher
    * data_dir_caller_request
    * data_dir_published_messsage
* Enable [touch screen support](https://www.raspberrypi.org/forums/viewtopic.php?p=947933) (if needed)


For auto start up of UAH service on boot
add the following line to /etc/xdg/lxsession/LXDE-pi/autostart

/home/pi/user_authentication_hub-middleware/startup_ISERE.sh

or

/home/pi/user_authentication_hub-middleware/startup_WOQUAZ.sh


**Localization**

The UI of UAH supports localization. So all texts can be translated to different languages.

We are using the [GNU gettext](https://www.gnu.org/software/gettext/) module in python.

All the strings in the hanlder.py are marked with an "_" e.g. print(_("Hello world")).

We generate the base .pot file and then you can edit them using [poedit](https://poedit.net/) and generate the .mo files.

The translation files should be saved in folder ui/locale/language/LC_MESSAGES/ 


**Acknowledgment**

This project has received funding from the European Union’s Horizon 2020 research and innovation programme under grant agreement No 732679
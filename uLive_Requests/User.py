
import xml.etree.ElementTree as ET


def extractUsers(xmltoParse):
    # create element tree object 
    users=[]
    tree = ET.fromstring(xmltoParse) 
    # get root element 
    #root = tree.getroot() 
    for child in tree:
      if child.tag == '{http://www.w3.org/2005/sparql-results#}results':
        for result in child:
          if (result.tag=='{http://www.w3.org/2005/sparql-results#}result'):
           for binding in result:
            if(binding.attrib['name']=='u'):
             el=binding.find('{http://www.w3.org/2005/sparql-results#}uri')
             idm=el.text
            elif (binding.attrib['name']=='n'):
             el=binding.find('{http://www.w3.org/2005/sparql-results#}literal')
             name=el.text
            elif (binding.attrib['name']=='g'):
             el=binding.find('{http://www.w3.org/2005/sparql-results#}uri')
             gender=el.text
           u=User(idm,name,gender)
           users.append(u)
    return users


class User():
 def __init__(self,Uid,Uname, Ugender):
  self.Uid=Uid
  self.Uname=Uname
  self.Ugender=Ugender

 def getID(self):
  return self.Uid
 def getName(self):
  return self.Uname
 def getGender(self):
  return self.Ugender

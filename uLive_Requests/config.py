import  os
from requests.auth import HTTPBasicAuth
from easydict import EasyDict as edict
import socket    

config = edict()
hostname = socket.gethostname()    
IPAddr = socket.gethostbyname(hostname) 

config.JsonHeader={'Content-type': 'application/json'}
config.TextHeader={'Content-type': 'text/plain', 'Accept': 'text/plain'}
config.Auth=HTTPBasicAuth('uLive', 'uLive')  # universaal rest api username/password
config.target_uSpace='uLive_space'  # space name
config.caller_instanceID='uah_caller'+IPAddr.replace('.','')     
config.publisher_instanceID='uah_publisher'+IPAddr.replace('.','')
config.serverIP='146.140.36.13'   # target universaal instance ip address 
config.serverPort='9000' #  universaal api  port 
config.base_dir='/home/pi/user_authentication_hub-middleware/uLive_Requests/data/'
config.data_dir_space='space.txt'
config.data_dir_caller='caller.txt'
config.data_dir_publisher='publisher.txt'
config.data_dir_caller_request='caller_request.txt'
config.data_dir_published_messsage='published_messsage.txt'






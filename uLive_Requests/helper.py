import requests
import logging
from requests.exceptions import HTTPError
from requests.auth import HTTPBasicAuth
from config import *
import time
import xml.etree.ElementTree as ET
from User import *

import re


def doJsonRequest(logger,api_endpoint, data, headers, auth):
    try: 
        req = requests.post(url = api_endpoint, data = data,  headers=headers,auth=auth )   # extracting response text  
        req.raise_for_status()
    except HTTPError as http_err:
        logger.info('HTTP error occurred: '+str(http_err))
        return None
    except Exception as err:
        logger.info('Other error occurred: '+str(err))
        return None
    else:
        return req

def uSpaceJoin(logger):
    api_endpoint='http://'+config.serverIP+':'+config.serverPort+'/uaal/spaces'
    with open(os.path.join(config.base_dir,config.data_dir_space),'r') as f:
        lines=f.read()
    message_body=lines.replace('${target-uSpace}',config.target_uSpace)
    result=doJsonRequest(logger,api_endpoint,message_body,config.JsonHeader,config.Auth)
    return result

def registerPublisher(logger):
    api_endpoint='http://'+config.serverIP+':'+config.serverPort+'/uaal/spaces/'+config.target_uSpace+'/context/publishers/'
    with open(os.path.join(config.base_dir,config.data_dir_publisher),'r') as f:
        lines=f.read()
    message_body=lines.replace('${target-uSpace}',config.target_uSpace).replace('${instanceID}',config.publisher_instanceID)
    result=doJsonRequest(logger,api_endpoint, message_body, config.JsonHeader, config.Auth)
    return result


def userIdentified(logger,userID, counter):
    api_endpoint='http://'+config.serverIP+':'+config.serverPort+'/uaal/spaces/'+config.target_uSpace+'/context/publishers/'+config.publisher_instanceID
    with open(os.path.join(config.base_dir,config.data_dir_published_messsage),'r') as f:
        message_body=f.read()
    message_body=message_body.replace('${instanceID}',config.publisher_instanceID)
    message_body=message_body.replace('${situationID}',userID)
    current_milli_time=lambda: int(round(time.time()* 1000))
    message_body=message_body.replace("${timestamp}", str(current_milli_time()))  
    message_body=message_body.replace("${counter}", str(counter))
    result=doJsonRequest(logger,api_endpoint, message_body, config.TextHeader, config.Auth)
    return result
